/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'templates/**/*.html.twig',
		'assets/js/**/*.js',
		'assets/js/**/*.jsx', // Si vous utilisez des fichiers React JSX
	],
	theme: {
		extend: {
			colors: {
				orange: {
					brand: '#c38113',
					hover: '#a36c0d',
				},
				red: {
					brand: '#8e0341',
					hover: '#4c0022',
				},
				gray: {
					brand: '#918f90',
				},
				pink: {
					brand: '#f7c1ba',
				},
				lightGray: {
					brand: '#e9e9e9',
				},
				black: {
					brand: '#2e2a2b',
				},
			},
		},
	},
	plugins: [],
};
