<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Entity\Event;
use App\Entity\User;
use App\Form\BlogType;
use App\Form\EventType;
use App\Repository\BlogRepository;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/evenements", name="show_events")
     */
    public function show_events(EventRepository $repo): Response
    {


        $events =  $repo->findAll();
        return $this->render('home/show_events.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * @Route("/evenement/create", name="create")
     * @Route("/evenement/{id}/edit", name="edit")
     */
    public function form(Event $event = null, Request $request, ManagerRegistry $manager)
    {

        if (empty($this->getUser())) {

            return $this->redirectToRoute('login');
        }


        if (!$event) {
            $event = new Event();
        }

        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);
        dump($form);

        if ($form->isSubmitted() && $form->isValid()) {
            // if (!$event->getId()) {
            $event->setCreatedAt(new \DateTime());
            $event->setUser($this->getUser());
            $event->setPlacesRemain($event->getNbPlaces());
            $event->setImage('new image');
            $event->setNbPlaces(1);
            $event->setEventDate('ok');
            // }

            $em = $manager->getManager();
            $em->persist($event);
            $em->flush();


            return $this->redirectToRoute('show_events', ['id' => $event->getId()]);
        }

        return $this->render('home/create.html.twig', [
            'form' => $form->createView(),
            'editMode' => $event->getId() !== null
        ]);
    }


    /**
     * @Route("/evenement/{id}", name="show")
     */
    public function show(Event $event)
    {

        return $this->render('home/show.html.twig', [
            'event' => $event
        ]);
    }

    /**
     * @Route("/evenement/{id}/delete", name="delete")
     */
    public function delete(EventRepository $repo, Event $event, ManagerRegistry $manager,)
    {

        $em = $manager->getManager();
        $em->remove($event);
        $em->flush();


        $events =  $repo->findAll();

        return $this->render('home/show_events.html.twig', [
            'events' => $events
        ]); //TODO : rediriger vers la liste des annonces
    }

    /**
     * @Route("/evenement/{id}/inscription", name="inscription_event")
     * @Route("/evenement/{id}/desinscription", name="desinscription_event")
     */
    public function inscriptionEvent(Event $event, Request $request, ManagerRegistry $manager)
    {


        if (empty($this->getUser())) {

            return $this->redirectToRoute('login');
        };

        $places = $event->getPlacesRemain();

        $getUser = $this->getUser();

        $entrant = $event->getEntrant();

        // dump($user->getParticipationEvents($getUser));

        // if (!$event->getEntrant($getUser)) {
        if ($event->getPlacesRemain() > 0) {
            $event->setPlacesRemain($places - 1);
            $event->addEntrant($getUser);
        } else {
            return $this->render('home/show.html.twig', [
                'plus_de_place' => 'Désolé l\'évènement est déjà complet.',
                'event' => $event,
                // 'entrant' => $entrant
            ]);
        }

        $em = $manager->getManager();
        $em->persist($event);
        $em->flush();
        // } else {
        //     $event->setPlacesRemain($places + 1);
        //     $event->removeEntrant($user);
        //     return $this->render('home/show.html.twig', [
        //         'inscrit' => 'Vous êtes déjà inscrit à cet evenement',
        //         'event' => $event,
        //         'entrant' => $entrant
        //     ]);
        // }


        return $this->render('home/show.html.twig', [
            'event' => $event,
            // 'entrant' => $entrant
        ]);
    }

    /**
     * @Route("/compte", name="account")
     */
    public function compte(EventRepository $eventRepository)
    {
        $id_event = $this->getUser();

        dump($id_event);

        $events = $eventRepository->findAllById($id_event);
        return $this->render('home/account.html.twig', [
            'eves' => $events,
            'user' => $id_event
        ]);
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function blog(BlogRepository $repo)
    {

        $blog = $repo->findAll();
        return $this->render('home/blog.html.twig', [
            'blogs' => $blog
        ]);
    }


    /**
     * @Route("/blog/create", name="create_blog")
     * @Route("/blog/{id}/edit", name="edit_blog")
     */
    public function formBlog(Blog $blog = null, Request $request, ManagerRegistry $manager)
    {

        if (empty($this->getUser())) {

            return $this->redirectToRoute('login');
        }


        if (!$blog) {
            $blog = new Blog();
        }

        $formBlog = $this->createForm(BlogType::class, $blog);

        $formBlog->handleRequest($request);
        dump($formBlog);

        if ($formBlog->isSubmitted() && $formBlog->isValid()) {

            $blog->setCreatedAt(new \DateTime());

            $em = $manager->getManager();
            $em->persist($blog);
            $em->flush();


            return $this->redirectToRoute('blog_show', ['id' => $blog->getId()]);
        }

        return $this->render('home/create_blog.html.twig', [
            'form' => $formBlog->createView(),
            'editMode' => $blog->getId() !== null
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show_blog(BlogRepository $repo, Blog $blog)
    {
        return $this->render('home/blog_show.html.twig', [
            'blog' => $blog
        ]);
    }

    /**
     * @Route("/blog/{id}/delete", name="delete_blog")
     */
    public function deleteArticle(BlogRepository $repo, Blog $blog, ManagerRegistry $manager,)
    {

        $em = $manager->getManager();
        $em->remove($blog);
        $em->flush();


        $blogs =  $repo->findAll();

        return $this->render('home/blog.html.twig', [
            'blogs' => $blogs
        ]); //TODO : rediriger vers la liste des articles
    }

    /**
     * @Route("/rgpd", name="rgpd")
     */
    public function rgpd()
    {
        return $this->render('home/rgpd.html.twig');
    }
}
