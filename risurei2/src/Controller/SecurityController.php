<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security")
     */
    public function registration(Request $request, ManagerRegistry $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $user->setAdministrateur(false);

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        dump($form);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $manager->getManager();

            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $em->persist($user);

            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/desinscription/{id}", name="delete_user")
     */
    public function delete_user(User $user, ManagerRegistry $manager)
    {
        $em = $manager->getManager();
        $em->remove($user);
        $em->flush();

        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/connexion", name="login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logout()
    {
    }
}
